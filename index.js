//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// Use an object literal
// ENCAPSULATION
// spaghetti code - code that is poorly organized that it becomes almost impossible to work with

let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    login() {
        console.log(`${this.email} has logged in`)
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
        console.log(`${this.name} 's quarterly grade averages are: ${this.grades}`)
    },

    /*
        MINI ACTIVITY
            create a function that will compute the quarterly average of studentOne's grades

    */
    totalAverage() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade );
        return sum/4;
    },
    
    /*
        MINI ACTIVITY
            create a function willPass() will return true if average is >=85, and false if not.
        

    */
    willPass() {

        return this.totalAverage() >= 85 ? true : false
       // const average = this.totalAverage();
       // console.log(`${studentOne.name} quarterly grade average is : ${average}`)
       // if ( average >= 85) {
       //  return true
       // } else {
       //  return false
       // }
     },

    /*

        MINI ACTIVITY
        create a function called willPassWithHonors() that returns true if the student has passed and their average grade is >= 90. the function returns false either one is met.

    */

    willPassWithHonors() {
           const average = this.totalAverage();

           if (average >= 90) {
               return true;
           } else if (average >= 85) {
               return false;
           } else if (average < 85){
               return undefined;
           }
       },




};
console.log({studentOne})
console.log(`student one's name is ${studentOne.name}`)
console.log(`student one's email is ${studentOne.email}`)
console.log(`student one's quarterly grade average are  ${studentOne.grades}`)

// ~0~~0~~0~~0~~0~~0~~0~~0~~0~~0~~0~~0~ACTIVITY~0~

/*

    1.   What is the term given to unorganized code that's very hard to work with
        *spaghetti code

    2.   How are object literals written in JS?
        * by using {} with key value pairs

    3. What do you call the concept of organizing information and  functionality to belong to an object?
        * OOP

    4.   If studentOne has a method named enroll(), how would you invoke it?
        *studentOne.enroll();

    5.   True or False: Objects can have objects as properties.
        * True

    6.    What is the syntax in creating key-value pairs?
        *  { key1: value1, key2: value2, ... }
    
    7.    True or False: A method can have no parameters and still work.
        *True

    8.    True or False: Arrays can have objects as elements.
        *True

    9.    True or False: Arrays are objects.
        *True

    10.   True or False: Objects can have arrays as properties.
        *True



*/

// create student two

let studentTwo = {
    name: 'Joe' ,
    email: 'joe@mail.com' ,
    studentTwoGrades: [78, 82, 79, 85],  
    totalAverage() {
        let sum = 0;
        this.studentTwoGrades.forEach(grade => sum = sum + grade );
        return sum/4;
    },

    willPass() {
       const average = this.totalAverage();
       console.log(`${studentTwo.name} quarterly grade average is : ${average}`)
       if ( average >= 85) {
        return true
       } else {
        return false
       }
    },

    willPassWithHonors() {
        const average = this.totalAverage();

        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else if (average < 85){
            return undefined;
        }
    },

};

let studentThree = {
    name: 'Jane' ,
    email: 'jane@mail.com' ,
    studentThreeGrades: [87, 89, 91, 93],  
    totalAverage() {
        let sum = 0;
        this.studentThreeGrades.forEach(grade => sum = sum + grade );
        return sum/4;
    },

    willPass() {
       const average = this.totalAverage();
       console.log(`${studentThree.name} quarterly grade average is : ${average}`)
       if ( average >= 85) {
        return true
       } else {
        return false
       }
    },

    willPassWithHonors() {
        const average = this.totalAverage();

        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else if (average < 85){
            return undefined;
        }
    },

};

let studentFour = {
    name: 'Jessie' ,
    email: 'jessie@mail.com' ,
    studentFourGrades: [91, 89, 92, 93],  
    totalAverage() {
        let sum = 0;
        this.studentFourGrades.forEach(grade => sum = sum + grade );
        return sum/4;
    },

    willPass() {
       const average = this.totalAverage();
       console.log(`${studentFour.name} quarterly grade average is : ${average}`)
       if ( average >= 85) {
        return true
       } else {
        return false
       }
    },

    willPassWithHonors() {
        const average = this.totalAverage();

        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else if (average < 85){
            return undefined;
        }
    },

};

const classof1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents() {
        const honorStudents = this.students.filter(student => student.willPassWithHonors() === true);
        return honorStudents.length;
    },

    honorsPercentage() {
        const totalStudents = this.students.length;
        const honorStudents = this.countHonorStudents();
        const percentage = (honorStudents / totalStudents) * 100;
        return percentage
    },

    retrieveHonorStudentInfo() {
        const honorStudents = [];
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                honorStudents.push({
                email: student.email,
                averageGrade: student.totalAverage()
            });
            }
        });
        return honorStudents;
    },

    sortHonorStudentsByGradeDesc() {
        const honorStudents = this.students.filter(student => student.willPassWithHonors() === true);
        const sortedStudents = honorStudents.sort((a, b) => b.totalAverage() - a.totalAverage());
        const studentInfo = sortedStudents.map(student => {
            return {
                email: student.email,
                averageGrade: student.totalAverage(),
            };
        });
        return studentInfo;
      }

}


